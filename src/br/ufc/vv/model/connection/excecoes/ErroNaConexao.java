package br.ufc.vv.model.connection.excecoes;

public class ErroNaConexao extends Exception{
	public ErroNaConexao(String erro) {
		super(erro);
	}
}