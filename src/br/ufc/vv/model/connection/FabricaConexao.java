package br.ufc.vv.model.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import br.ufc.vv.model.connection.constantes.ConstanteConfiguracao;
import br.ufc.vv.model.connection.excecoes.ErroNaConexao;

public class FabricaConexao {
	public Connection getConexao() throws ErroNaConexao{
		try{
			Class.forName(ConstanteConfiguracao.CLASS_NAME);
			return DriverManager.getConnection("jdbc:mysql://"+ConstanteConfiguracao.HOST+"/"+ConstanteConfiguracao.BANCO,ConstanteConfiguracao.USUARIO, ConstanteConfiguracao.SENHA);
		}catch(ClassNotFoundException | SQLException e){
			throw new ErroNaConexao(e.getMessage());
		}
	}
}
