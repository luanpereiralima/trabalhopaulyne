package br.ufc.vv.control.contract;

import java.util.Iterator;
import br.ufc.vv.control.excecoes.ErroParametros;
import br.ufc.vv.model.connection.excecoes.ErroNaConexao;
import br.ufc.vv.model.contract.IFilme;
import br.ufc.vv.model.contract.IPessoa;
import br.ufc.vv.model.excecoes.ErroDAO;

public interface IFilmeController {
	
	public void criarFilme(String titulo, String sinopse, String genero, 
			String anoFilmagem, String anoLancamento, String estudioFilmagem) throws ErroNaConexao;
	
	public void editarFilme(int id,String titulo, String sinopse, String genero,
			String anoFilmagem, String anoLancamento, String estudioFilmagem) throws ErroDAO, ErroNaConexao;
		
	public void removerFilme(int idFilme) throws ErroNaConexao, ErroDAO;
	
	public void addAtoresDoFilme(int idFilme, Iterator<IPessoa> atores) throws ErroNaConexao, ErroDAO, ErroParametros;
	
	public Iterator<IPessoa> getAtoresDoFilme(int idFilme) throws ErroNaConexao, ErroDAO, ErroParametros;
	
	public void setDiretorDoFilme(int idFilme, IPessoa diretor) throws ErroNaConexao, ErroDAO, ErroParametros;
	
	public IFilme buscarFilmePorId(int id) throws ErroNaConexao;

	public IFilme buscarFilmePorTitulo(String titulo) throws ErroNaConexao;
	
	public Iterator<IFilme> listarFilmes() throws ErroNaConexao;
}
