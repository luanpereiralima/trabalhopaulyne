package br.ufc.vv.control.excecoes;

public class ErroParametros extends Exception{
	public ErroParametros() {
		super("Parametros nulos");
	}
}
